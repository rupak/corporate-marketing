## Quick links
* Primary channel: [LinkedIn](https://www.linkedin.com/company/gitlab-com/)
* GitLab brand account schedule here: *link schedule issue*
* Image to download: *add updated design assets*
* Tracked links for assets:
  * *add utm links for all campaign content* 

## Description
This is the issue to get everything you need (and ask any questions you have) in order to amplify the *campaign name* on LinkedIn! 

We're focusing on LinkedIn for the Sales team social because traditionally, content for our buyers performs much better on LinkedIn. This suggests that your most relevant audience members are more active on LinkedIn than Twitter, for example. You're very welcome to also promote on other channels, but this issue will focus on LinkedIn. 

## What you can do
* Share posts from the GitLab brand account
* Create your own posts!
  * Re-use copy from our posts 
  * Tweak copy from our posts
  * Write your own copy based on your insights from the field! 

## Things to keep in mind
* **Length:** You aren't bound by character count the same way you are on Twitter, so you can think about LinkedIn posts as mini blog posts. This is especially effective if you have a really good anecdote that you think might complement the asset you're promoting. However, keep in mind that, unless it's *really* interesting, people won't click `more` on your copy, so keep it on the short side unless you have a great hook above the fold. Longer posts are easier to read if you have a lot of paragraph breaks, so feel free to be creative with your formatting - one liners, emojis, bullet points, etc. can help break up your text visually. 

* **Tone:** That said, sense check your copy. The most important thing is that it sounds like you, a human being. Overly contrived posts generally do not do well. If loading up your post with emojis isn't your style, don't force it. Try to step back from sales mode! Your social copy isn't the place to close a deal, it's the place to tease the content by sharing an intriguing detail or riff that makes your post irresistible. 

* **Visuals:** If you do *one thing* on this list, let it be that you upload an image (above) to replace the generic tanuki image automatically pulled into your social card. This makes a huge difference for impressions (and clicks), so it's worth the hassle. 

* **Tracked links:** In order for us to measure the traffic you're driving, it is paramount that you add a tracked link. If you reshare GitLab posts, or use the links above to build your own posts, then you'll be safe. 