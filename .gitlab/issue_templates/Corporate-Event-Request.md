#### Request an event:
This section is to be used by anyone internally requesting GitLab sponsor a corporate event. Not sure what qualifies, please visit see the [events decision tree](https://docs.google.com/spreadsheets/d/1aWsmsksPfOlX1t6TeqPkh5EQXergt7qjHAjGTxU27as/edit?usp=drive_web&ouid=110206726155880220171).

# Instructions
Please fill all the necessary information out. If you don’t need a section, feel empowered to delete it from the issue. 
* title the issue: "Name of event", Location, Date of Event
* Make the due date the day the event starts
* Name of event:
* Event website: 
* Justification for wanting to sponsor the event (please be as detailed as possible): 
* If we went last year, link to SF.com campaign:  
* `Assign the Quarter the work will happen in, and Region tags to the issue`

 ---
 **Everything below this section will be updated once the event has been approved. All this info is needed for budget approval and to move into contracting** 
 
* [Epic]() Add Epic Link here- to be added by MPM
* [Event Planning Sheet]() copy this doc and create own version for specific event- https://docs.google.com/spreadsheets/d/1rXzp1bLoiI-IJ86Jbe39Kif2iujQoWVNXsbxQisFwUY/edit#gid=1085564346
* [Budget Doc](https://docs.google.com/spreadsheets/d/1R5wJuG8H-DfgkbSOJYhGs-YvTEGFR2DZX9KN_VegG7o/edit?ts=5de975d1#gid=0) - `It is the responsiobilty of the DRI to keep the budget doc up-to-date and to notify their manager as soon as or if you think you will go over budget` 

## :notepad_spiral: Event Details 
* **FMM/Event Owner:** CEM Name @corpeventusername
* **MPM:** @mpmusername
* **Type:** (select one) Conference, Owned Event, Other - [*see handbook for event type definitions*](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/#offline-events-channel-types)
* **Official Event Name:** 
* **Date:**
* **Location:** 
* **Event Website:**
* **Expected Number of Attendees at the event:**
* **GitLab Staffing Needs:**
* **Dress Code/ Attire:**
* **GitLab Hosted Landing page (if applicable):**
* **Speakers (if applicable):**
* **SFDC Campaign:**
* **Attendee List/ Audience Demographics:**
* **If we went last year, link to SF.com campaign:** 
* **Event Goals:**
  * Goals for # of leads: 
  * Goals for pipeline created $$$: 
  * Number of meetings at event: 
  * registration goals/ attendance goals: 
* **Campaign tag (requested & created by FMM in **Finance Issue** - [ISOdate_Campaign_ShortName](https://about.gitlab.com/handbook/marketing/#campaign-tags)):**
* **Budgeted costs (sponsorship + auxiliary cost (AV, booth, swag, travel, printed materials...)):** `DRI to fill in`

### Details of Sponsorship
* **Sponsorship (if applicable):** outline level, benefits and cost breakdown.

`**Once this section is complete you can go into contracting. To beging budget approval and contract review start an issue in the [finance project](https://gitlab.com/gitlab-com/finance/issues) using the "vendor_contracts_marketing" tenmplate. Be sure to include any concessions you have negotiated or discounts we are getting in step to of the template.**`


---
`**For Event Planners to complete for MPM once the contract signed**`

#### What will this event contain/require? - if "MAYBE" please indicate estimated date of decision.
⚠️ **This section must be filled out by the FMM before the MPM creates the epic and related issues. [See handbook.](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs/#events-process-for-marketing-programs--marketing-ops)**
* Follow up email via Marketo - YES/NO
* Will the copy rely on content from the event? i.e. copy due 24 hours post-event (this will push out the send 2 business days). - YES/NO
* Add event leads to nurture stream? - YES/NO (please specify nurture stream) [see nurture options here](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs/#top-funnel-nurture)
* Meeting/demo request landing page creation (requires promotion plan) - YES/NO (determine meeting/demo)
* Invitation and reminder emails (requires promotion plan) - YES/NO
* Speaking session - YES/NO/MAYBE
* Workshop - YES/NO/MAYBE
* Dinner - YES/NO/MAYBE
* Party/Happy Hour - YES/NO/MAYBE
* Alliances/Partner Marketing involved - YES/NO/MAYBE

---
`**For Event Planners complete once event contract signed- this is for our own internal tracking purposes**`

### Pre- Event Checklist (not every item below will be relevant to every show. Delete any items not necessary)
* [ ] Contract Signed and counter signed contract aedded to ContractWorks. [Link to contract issue]()
* [ ] Event added to Events Cal and Events page
* [ ] Issue status changed from Plan to WIP- `this triggers MPM to create epic and all relevant issues`
* [ ] Invoice paid- send invoice to  ap@gitlab.com and tag marketing ops for budgeting approval, include link to Epic with finance tag. All vendors must be setup in billing portal. Ping Lori in finance if you need assitance. 
* [ ] Create and share planning spreadsheet (included travel, booth duty, meetings...) from [planning sheet template](https://docs.google.com/spreadsheets/d/1rXzp1bLoiI-IJ86Jbe39Kif2iujQoWVNXsbxQisFwUY/edit#gid=1085564346)
* [ ] Slack channel created and attendees invited (Naming conventuon for channel- use event name, location, and year. link to Epic in channel description)
* [ ] Staffing selected and Tickets allocated   
* [ ] Decide on if we will do swag and any other printed materials (see swag selcetion below if you are shipping swag)
* [ ] Attendee directory from organizers (it is not common to get the list of attendees)
* [ ] Press list sent to PR team
* [ ] Booth duty scheduling (add to spreadsheet and send individual cal invites)
* [ ] Social media copy written and scheduled - there is an issue template for this (give them as much notice as possible)
* [ ] Flights/ transport/ lodging booked- added to spreadsheet
* [ ] Booth slideshow/ demo (shared with staff)- work with PMM on any specific decks/ demos
* [ ] Final prep meeting scheduled (end of week before the show begins. Include everyone attending, planning and person from PMM who will do demo training)
* [ ] Share booth deck on slack channel- best to share published, shortened url
* [ ] Event post mortem scheduled (1-2 weeks post event)

### Event Content/ Copy
* [ ] Copy provided to MPM- we are getting a dedicated respource for this but DRI is in charge of makeing sure deadlines are met
   * [ ] Landing page copy (decide if there will be a landing page and if it will need a meeting setting form)
   * [ ] Pre event email (3-4 weeks before show)
   * [ ] Reminder copy
   * [ ] Post event emails (1 week before show)

### To be done in ERC or directly with Event organizers
* [ ] Artwork sent- this is for posting on the event page and on site signage
* [ ] Company description (use [Value Prop Messaging](https://about.gitlab.com/handbook/marketing/product-marketing/messaging/#gitlab-value-proposition). Ping PMM if you need something longer or specific)
* [ ] Badge scanners- get 1 for around every 1000 people at the show, but do not exceed amount of people we have staffing the booth at one time. 
* [ ] Other

### Booth
* [ ] Decide on which type of booth should be used at event:
* [ ] AV ordered- monitors
* [ ] Furnishings ordered
* [ ] Electrical if needed
* [ ] Power if not incuded in package or need more
* [ ] Booth Design Issue created- design needs at least 2 weeks notice to design (see handbook]([Epic](https://about.gitlab.com/handbook/marketing/corporate-marketing/#corporate-event-execution-process)
* [ ] Booth Artwork submitted 
* [ ] Final mock up approved

### Speaker Checklist
* [ ] Create speaker request template in corporate marketing and tag Tech Evangelism (and Alliances if this is a Partner event)
* [ ] Speaker brief + bio sent to roganizers or through ERC portal 
* [ ] Slides reviewed by content and design
* [ ] Speaker Slides Sent (Deadline: )
* [ ] Speaker final run through scheduled
* [ ] Talks added to cal and planning sheet

### At Event Meeting Plan (w/ Mktg OPS and FM support)
* [ ] Decide on meeting setting plan with regional FM rep, designated MPM, and EA team (when Exec involved)
* [ ] Target customers/ prospects identified
   * [ ] Share previous lead lists with team on plannign sheet if we have them
   * [ ] Pull speaker list
* [ ] [Client Meeting Prep Template](https://docs.google.com/document/d/1QZB8m99Lt5GVnBDcfST1otmA81uH6EMEMkF-1zhQ0q0/edit#) must be completed for every on site meeting by AE

---
### At Event
* [ ] Check on booth
  * [ ] monitor(s) there and in right place
  * [ ] artwork printed correctly
  * [ ] all rentail items arrived
  * [ ] swag arrived
* [ ] pick up badge scanners
* [ ] send pic of booth ro be sent out on social 
* [ ] Keep both tidy, everyone on scheudle

---
### Post Event:
* [ ] Leads shared with MPM and ops `24 hours after event close`
* [ ] Leads uploaded to campaign and list locked (all changes after lock to be made in SFDC)
* [ ] Sales notified of lead/contact assignments - auto-assign to match named accounts 
* [ ] After event follow up launched
* [ ] After event survey sent 

---
### Swag checklist:
* [ ] swag ordered
* Shipping Address:
* In hand date and time:
* Item/ Color/ Volume:
* Price per item:
* Total $: 
* Tracking: 

* Mens
  
| XS | Sm | Med | Lg | XL | XXL | XXXL |
|----|----|-----|----|----|-----|------|
|    |    |     |    |    |     |      |
  
* Women's
   
| XS | Sm | Med | Lg | XL | XXL | XXXL |
|----|----|-----|----|----|-----|------|
|    |    |     |    |    |     |      |


cc @emily

/assign @nbsmith @emily

/label ~Events ~"Field Marketing" ~"MktgOps - FYI" ~"MPM - Radar" ~"Marketing Programs" ~"Marketing Campaign" ~"Corporate Event" ~"status::plan" ~"META"
