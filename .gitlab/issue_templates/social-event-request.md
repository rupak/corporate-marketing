### Request for event related organic (non-paid) social promotion for **[add event name]**

<!-- Requester please fill in all sections above the solid line. Some details may not be applicable. -->
### Event Details
Event date(s) or go live date:\
**[add date(s) here]**

Is there a landing page yet? Is it live?\
**[add page link, other details, here]**

What objectives do you want users to follow?\
*Think clicks, sign ups, awareness, etc.*\
**[add objectives here]**

Are there relevant hashtag(s)?\
*Please only include strategic conversations that align with your particular event.*\
**[add hashtag(s) here]**

Are there handles/profiles you'd like the social media team to follow/monitor before/during your event?\
**[add links to handles/profiles here]**

Anything else we should know or could be helpful to know?\
**[add additional thoughts here]**

###  To-Do's Once You've Created the Issue
   - [ ] Please link to all related issues and the epic

-----
### Design Needs
Social team will @ Luke if original images are needed
   - [ ] Twitter image (16:9 aspect ratio, min. 1200x675)
   - [ ] Facebook image (1200x630, less than 20% of image can have text) (test the Facebook Image to confirm: https://www.facebook.com/ads/tools/text_overlay)
   - [ ] LinkedIn image (1104x736)

------
### Social Schedule
Social team will fill in\
Please note that business needs, crisis situations, or other demands may require us to deviate from the schedule.
   - [ ] Post 1  
   - [ ] Post 2
   - [ ] Post 3

/label ~Events ~"Social" ~"status::plan"

/assign @ksundberg @wspillane