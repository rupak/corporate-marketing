<!-- Requestor, please open this issue, link the campaign/project issue and/or epic, and leave the rest empty. The social team will ask any further questions if needed. Thank you! -->

<!--Everything below this line is for the social team to fill out. -->

## Grab 'n Go Needs
* Primary channels: [Twitter](https://twitter.com/gitlab) & [LinkedIn](https://www.linkedin.com/company/gitlab-com/)
* Images
   *  `PENDING`
   *  If unique assets are needed, [create a new issue and use the design-request-general template](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues)
* Videos
   *  `PENDING`
* Use the appropriate link below when sharing the `PENDING`
   * Twitter: `PENDING`
   * LinkedIn: `PENDING`
   * Facebook: `PENDING`
*  Event Hashtag
   *  `PENDING`
   *  Appropriate to use across all channels

## Overview 
`EVENT OR CAMPAIGN NAME AND OVERVIEW`

**This is the issue** to get everything you need in order to amplify `EVENT OR CAMPAIGN NAME` on social.

If you decide to contribute to the conversation on social media, the social + PR teams want you to be equipped with the tools to make your inclusion on social media turn out for the best. That's why we've outlined our [social media guidelines for use of the @gitlab handles](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/).

**GitLab Team Members are not required to share, engage with, or comment on posts from @GitLab or publish your own posts on your own channels. The Social + PR teams will not be acting in an oversight capacity, so there is no company reviewing of your tweets before you decide to publish them.**

If you would like to connect with a team member from social or PR as a resource for any reason or if you'd like to keep up with @GitLab posts throughout the week of events, consider joining the `#social_media` Slack channel for an easy way to connect with questions and stay up to date on our latest posts.

## Goals
`PENDING`

## Perspective 
`PENDING`

## Hashtags to Use & Topics to Follow

Hashtags are most widely used on Twitter, though they can also have a place in posts on LinkedIn and Instagram. In most cases, hashtags are usually derived from a topic to discuss. 

Consider using a hashtag when you're posting about one of these topics:

`ADD HASHTAGS`

Remember that hashtags can help a wider audience of people see your posts, as people can follow/lookup hashtags and tend to do so during events. The people you want to connect with most during an event are inside your hashtag use.

## Writing Your Own Posts
Here are a few things you can do
* Retweet the GitLab brand channel `ADD LINK OR GUIDANCE`
* Share your own post with the appropriate link
   *  (see some examples below)

**General Examples to help get you inspired**
>  `INSERT EXAMPLE`

>  `INSERT EXAMPLE`

`EVENT OR CAMPAIGN SPECIFIC` examples

> `INSERT EXAMPLE`

> `INSERT EXAMPLE`

## Tips for Writing Your Own Posts
**Twitter Length**\
If your copy is on the longer side, try to break it up visually into paragraphs or one-liners, even using emojis/bullets. If you have something longer than 280 characters, like an anecdote or a schedule, consider composing a thread. [Learn how to make a Twitter Thread here](https://help.twitter.com/en/using-twitter/create-a-thread).

**LinkedIn Length**\
You aren't bound by character count the same way you are on Twitter, so you can think about LinkedIn posts as mini blog posts. This is especially effective if you have a really good anecdote that you think might complement the asset you're promoting. However, keep in mind that, unless it's *really* interesting, people won't click `more` on your post (the only way to read an entire post longer than a few lines), so keep it on the short side unless you have a great hook above the `more` button.

Longer posts are easier to read if you have a lot of paragraph breaks, so feel free to be creative with your formatting - one-liners, emojis, bullet points, etc. can help break up your text visually.

**Tone**\
That said, do sense check your copy. The most important thing is that it sounds like you, a human being. Overly contrived posts generally do not do well. If loading up your post with emojis isn't your style, don't force it. 

**Visuals**\
Add an image or video! This makes a huge difference for impressions (and clicks), so it's worth the hassle. Feel free to experiment if you have an idea to personalize it in a relevant way.

**Style**\
Don't worry about writing posts that sound like news or have an editorial perspective, the brand channels will have that covered. You should write like you're at #GitLabCommit and you're learning/loving/doing something new and fun - personalize the message. 

Instead of sharing a quote from a speaker, consider sharing the quote and adding why it personally resonated with you.

**Make Links Trackable**\
In order for us to measure the traffic you're driving, it is paramount that you add a tracked link. If you retweet GitLab posts, you're already safe. The links we provided above in the "Grab 'n Go" section are prefixed with the UTMs needed, so you can copy + paste them.

## A Thought on Arguments

Many of us have seen (or have been a part of) a conversation on a social channel that went way too far down the hole. The people in the chat are yelling over each other on the internet. There are too many responses and it's easy to see as someone who's not in the argument that it should have ended several responses ago. 

Sometimes, the best course of action is to walk away and not engage with the person at all. Use your judgment in how you approach rude or off-putting comments from strangers in real life to help you decide.

For a foundational understanding of these nuances, read [GitLab's guide to communicating effectively and responsibly through text](/company/culture/all-remote/effective-communication/).

**If you are unsure of how or if to respond to someone who has responded to your posts, consider joining the `#social_media` Slack channel for an easy way to connect with the social + PR team for help.**

## FAQ

**Am I required to share/like a post from @GitLab social channels?**\
No, you're not required to participate in any way.

**Am I required to have the social + PR team review my posts before I publish them?**\
No, this type of oversight directly contradicts our [results value, focused on giving our team members agency](https://about.gitlab.com/handbook/values/#give-agency). The GitLab social + PR team will never require a review or another oversight of your own personal social media posts.

**Am I required to identify myself as a GitLab employee on my social media channel?**\
No, you are not required to identify yourself as an employee.

**I don't want to write my own posts. I'd rather share @GitLab's social posts. Is there an easy way to know when they are published?**\
Yes, you can join the `#social_media` Slack channel for easy access to our latest posts as they publish.

**I don't know how to respond to a comment someone posted to me. How can I find help?**\
Consider joining the `#social_media` Slack channel for an easy way to connect with the social + PR team for help.

**What should I do if a friend/follower/user responds to me about controversies?**\
Consider not engaging with that person and ignoring their comments. The probability of "Twitter arguments" around various crises and controversies is high and it is recommended that you don't fall into a fight with someone on the internet.

**What should I do if a friend/follower/user responds to me about GitLab's customer agreement, the TOS/Telemetry update, the job family country block discussion, or another difficult topic?**\
Consider two options:
1. You could choose to not engage. You are not required to answer for anyone else or the company at large unless you feel comfortable enough to do so. If you'd like to respond but want help, consider joining the `#social_media` Slack channel for an easy way to connect. For any comments on the job family country block, consider not engaging as we currently do not have a position to share.
1. You can forward along GitLab positions on these topics as follows:
*  Customer Agreement:\
After feedback from the community, we made updates to our customer agreement. You can see the changes here: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/32628/diffs
*  TOS/Telemetry Updates:\
 We made a mistake. Check out our full position along with updates here. https://gitlab.com/gitlab-org/growth/product/issues/164

**Where can I learn more about social media for our company?**\
You can find our @GitLab Social Media Guidelines [here](https://about.gitlab.com/handbook/marketing/social-media-guidelines/) and the social media handbook [here](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/). Note, these guidelines were created for company use, but they may have some good information for you. (And note, these two separate locations will be updated into one in the future)

<!-- Please leave the label below on this issue -->

/label ~"Social" ~"status::plan"

/assign @wspillane