Title: Customer case study - [Account Name]

**Mark the issue as confidential**

### Account Overview

+ **Account Name**:
+ **Industry Vertical**: 
+ **Primary Contact/Champion's Name**: 
+ **Champion's Title**:
+ **Champion's Timezone**:
+ **Requested date-time for intro call**:

### Pre-call Resources

+ **Current GitLab products and subscription**: (e.g. CE, EES , EEP, PS, Geo, CI etc.) 
+ **Reason they went with GitLab**: 
+ **List competitors we won this account from**: (e.g. CE, Legacy SCM, BitBucket, GitHub, Jenkins, etc.)  
+ **Latest version.gitlab ping**:
+ **Existing Public mentions of GitLab or other tools**:
+ **Suggested case study focus area**: 

#### Copy and paste the label and assign commands into a comment  

This will add the "customer story" label and assign the issue to Erica (MCM) to manage the case study creation process.

 /label ~"customer story"
 
 /assign @KimLock
