## Event Details

* Name of event: 
* Event website: 
* Location: 
* Event Date:
* CFP Deadline: 

## Speaker / Proposal  Details
* Speaker: 
* Title: 
* One sentence description: 
* Abstract (can be a link to GDoc):
* Speaker Bio: 
* Other Info (e.g. length, takeaways, target audience):

## Additional speaker information (for non-GitLabbers)
* Company name: 
* How big is your company?: 
* Title (Position):

## Submission Status
* [ ] WIP - please send to Priyanka for review before submitting. 
* [ ] Submitted to Conference
* [ ] Accepted / Rejected / Waitlist

## For accepted proposals 
* [ ] Added to events page
* [ ] Speaker brief + bio sent
* [ ] Request for slide support
* [ ] Slides reviewed by content and design
* [ ] Speaker slides sent (Deadline: )
* [ ] Speaker final run through scheduled
* [ ] Promotion support (please detail how you would Corporate Marketing to promote the talk)

For your input please: @Emily @kuthiala @erica @johncoghlan @pritianka @echin 

/label ~Events ~Speaker ~SPIFF ~"status::plan"