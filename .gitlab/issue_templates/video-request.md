⚠️ **All video production requests require a minimum of 8 weeks lead time!** ⚠️

# Requestor section 

* [ ]  **Please link this request issue to the corresponding epic.**
* [ ]  **Please link all relevant docs, e.g. meeting and planning docs, to this issue.**

## Type of request 

Choose all that apply. Please reference the [video production handbook](#) for type definitions. 

* [ ]  Customer/community/user story
* [ ]  Editorial 
* [ ]  Event AV production
* [ ]  Internal event (e.g. SKO)
* [ ]  Livestream
* [ ]  On-site interviews
* [ ]  Panel recording
* [ ]  Promotional (e.g. GitLab branded event, Culture, Employment Branding, Product)
* [ ]  Speaker/presentation recording (e.g. Connect)
* [ ]  Technical evangelism

## Request details (requestor to fill out)

- **Filming date(s):**
- **Location:**
- **Campaign and/or event:** 
- **# of expected videos:** 
- **Expected cost:** 
- **Are there any presentation decks associated with this video (please link here)?**

### Description of video needs:

<!-- Requestor to fill out in detail. Let us know what you're aiming to accomplish, 
what/who you hope to capture, and how the final content will be used. -->

`Talent list:` <!-- Who are we filming? --> 

| First name | Last name |  Title | Company | Link to deck/interview questions | 
| ------ | ------ | ------ | ------ | ------ |
| {{first name}} | {{last name}} | {{title}} | {{company}} | [link](#) |

`List of final video assets:`

1. e.g topical video package
1. e.g. {{First name}} {{Last name}} speaker session 

### Venue requirements

*Please link all relevant venue documents* 

- **Have you acquired permission to film at the venue?**
- **Does the venue require production insurance?**
- **Does the venue require release forms?**
- **Are there any filming restrictions (If yes, please details restrictions here or attach relevant documentation)?**
- **Does the venue have a PA or AV system in-house?**
- **Please provide details on the room set up (photos, website link, etc are helpful)**: 

## Event schedule

<!-- If video is part of an event, please list or link to the day of schedule of events. --> 

# Production section 

cc @erica

/assign @atflowers

/label ~"Digital production"



