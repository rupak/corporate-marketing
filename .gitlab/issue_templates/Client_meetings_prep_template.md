# Client meetings prep template

**Meeting Date:**     
**Meeting Time:**      
**Meeting location:**      
**Company:**       
**Account Owner(s):**      

GitLabbers Present:

Meeting with (names and titles of those in attendance)

Background: (provide info on what we are taking this meeting, license count, pain points, tool stack…)

Customer focus:

GitLab focus:

EA contact:
